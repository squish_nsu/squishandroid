package ru.nsu.fit.squish.Authorized;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import ru.nsu.fit.squish.Authorized.Order.EmptyOrderFragment;
import ru.nsu.fit.squish.Authorized.Order.NavigationOrderFragment;
import ru.nsu.fit.squish.R;

public class AuthorizedActivity extends AppCompatActivity {

    private NavController navController;
    private BottomNavigationView navigationView;

    Fragment fragment = NavigationOrderFragment.newInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorized);

        navigationView = findViewById(R.id.bottomNav);
        navController = Navigation.findNavController(this, R.id.nav_authorized);
        NavigationUI.setupWithNavController(navigationView, navController);
    }
}