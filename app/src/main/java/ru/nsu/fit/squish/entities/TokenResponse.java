package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenResponse {
    @SerializedName("userId")
    private Integer id;

    @SerializedName("refresh")
    private String refresh;

    @SerializedName("access")
    private String access;

    public Integer getId() {
        return id;
    }

    public String getRefresh() {
        return refresh;
    }

    public String getAccess() {
        return access;
    }
}
