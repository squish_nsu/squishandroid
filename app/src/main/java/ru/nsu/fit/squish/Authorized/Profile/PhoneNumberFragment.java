package ru.nsu.fit.squish.Authorized.Profile;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.AuthInfo;
import ru.nsu.fit.squish.network.Requests;
import ru.nsu.fit.squish.network.TokenManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PhoneNumberFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PhoneNumberFragment extends Fragment {

    private EditText oldPhoneNum;
    private EditText newPhoneNum;
    private EditText password;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PhoneNumberFragment() {
        // Required empty public constructor
    }

    public static PhoneNumberFragment newInstance(String param1, String param2) {
        PhoneNumberFragment fragment = new PhoneNumberFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_phone_number, container, false);

        oldPhoneNum = view.findViewById(R.id.oldPhoneNumber);
        newPhoneNum = view.findViewById(R.id.newPhoneNumber);
        password = view.findViewById(R.id.editPassword);

        ImageButton backToProfile = view.findViewById(R.id.backToProfile);
        backToProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.profileFragment2);
            }
        });

        Button button = view.findViewById(R.id.onChangeNumber);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String errors = checkFields();
                if(!errors.isEmpty()) {
                    Toast.makeText(getContext(), errors, Toast.LENGTH_SHORT).show();
                    return;
                }

                if (oldPhoneNum.getText().toString().equals(newPhoneNum.getText().toString())) {
                    Toast.makeText(getContext(), "Новый номер телефона совпадает со старым", Toast.LENGTH_SHORT).show();
                    return;
                }

                TokenManager tokenManager = new TokenManager(v.getContext());
                int myId = tokenManager.fetchId();

                Requests.updatePhone(myId, new AuthInfo(newPhoneNum.getText().toString(), password.getText().toString()), new Callback<AuthInfo>() {
                    @Override
                    public void onResponse(Call<AuthInfo> call, Response<AuthInfo> response) {
                        if (response.code() != 200) {
                            Toast.makeText(getContext(), "Непредвиденная ошибка", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        AuthInfo authInfo = response.body();

                        Bundle args = new Bundle();
                        args.putString("phoneNumber", newPhoneNum.getText().toString());

                        Navigation.findNavController(v).navigate(R.id.confirmNumberFragment, args);
                    }

                    @Override
                    public void onFailure(Call<AuthInfo> call, Throwable t) {

                    }
                });
            }
        });
        return view;
    }

    public String checkFields() {
        StringBuilder stringBuilder = new StringBuilder();
        if(oldPhoneNum.getText().toString().isEmpty()) {
            stringBuilder.append("Пустой старый номер телефона\n");
        }
        if(newPhoneNum.getText().toString().isEmpty()) {
            stringBuilder.append("Пустой новый номер телефона\n");
        }
        if(password.getText().toString().isEmpty()) {
            stringBuilder.append("Пустой пароль\n");
        }
        return stringBuilder.toString();
    }
}