package ru.nsu.fit.squish.network;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import ru.nsu.fit.squish.entities.AccountInfo;
import ru.nsu.fit.squish.entities.AuthInfo;
import ru.nsu.fit.squish.entities.UpdatePassInfo;
import ru.nsu.fit.squish.entities.ConfirmInfo;
import ru.nsu.fit.squish.entities.CreateOrderInfo;
import ru.nsu.fit.squish.entities.EndOrderInfo;
import ru.nsu.fit.squish.entities.LoginInfo;
import ru.nsu.fit.squish.entities.Order;
import ru.nsu.fit.squish.entities.RentalPoint;
import ru.nsu.fit.squish.entities.TokenResponse;

public interface SquishApi {

    @POST("registration/")
    Call<LoginInfo> register(@Body AuthInfo authInfo);

    @POST("registration/confirm/")
    Call<TokenResponse> confirm(@Body ConfirmInfo confirmInfo);

    @POST("token/")
    Call<TokenResponse> login(@Body AuthInfo authInfo);

    @POST("token/refresh/")
    Call<TokenResponse> refresh(@Body String refresh);

    @PUT("accounts/{id}/")
    Call<AuthInfo> updatePhone(@Path("id") int userId, @Body AuthInfo authInfo);

    @POST("code/")
    void code(@Body int code);

    @GET("rental_points/")
    Call<List<RentalPoint>> getAddresses();

    @POST("orders/")
    Call<Order> createOrder(@Body CreateOrderInfo createOrderInfo);

    @POST("orders/{id}/start/")
    Call<Order> startOrder(@Path("id") int id);

    @POST("orders/{id}/end/")
    Call<Order> endOrder(@Path("id") int id, @Body EndOrderInfo endOrderInfo);

    @POST("orders/{id}/pay/")
    Call<Order> payOrder(@Path("id") int id);

    @GET("orders/{id}/")
    Call<Order> getOrder(@Path("id") int id);

    @GET("orders/")
    Call<List<Order>> listOrders();

    @PUT("accounts/update_password/")
    Call<TokenResponse> updatePassword(@Body UpdatePassInfo changePassInfo);

    @GET("accounts/{id}/")
    Call<AccountInfo> accountsRead(@Path("id") int id);
}