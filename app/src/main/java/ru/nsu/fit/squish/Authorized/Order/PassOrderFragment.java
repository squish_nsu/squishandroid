package ru.nsu.fit.squish.Authorized.Order;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.Order;
import ru.nsu.fit.squish.network.Requests;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PassOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PassOrderFragment extends Fragment {

    private SharedPreferences pref;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PassOrderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PassOrderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PassOrderFragment newInstance(String param1, String param2) {
        PassOrderFragment fragment = new PassOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pass_order, container, false);
        TextView sizePaid = view.findViewById(R.id.sizePass);
        TextView destination = view.findViewById(R.id.destinationAddress);
        TextView needToPay = view.findViewById(R.id.needToPay);

        pref = this.getActivity().getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);
        int orderId = pref.getInt("orderId", 0);
        Requests.getOrder(orderId, new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                if(response.code() == 200) {
                    Order curOrder = response.body();
                    sizePaid.setText(curOrder.getBootPair().getSize());
                    int needToPayInt = getAdditionalPrice(getMinuteDiff(curOrder.getDateStarted()));
                    String text = needToPayInt + "₽";
                    needToPay.setText(text);
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                Toast.makeText(view.getContext(),"Ошибка сервера", Toast.LENGTH_SHORT).show();
            }
        });

        String textAddress = pref.getString("selectedAddress", "");
        destination.setText(textAddress);

        TextView paid = view.findViewById(R.id.paidPass);
        paid.setText(String.valueOf(pref.getInt("price", 0)));

        Button doneButton = view.findViewById(R.id.doneButton);
        doneButton.setOnClickListener(v -> {
            Requests.payOrder(orderId, new Callback<Order>() {
                @Override
                public void onResponse(Call<Order> call, Response<Order> response) {
                    if(response.code() == 200) {
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putInt("state", 0);
                        editor.remove("selectedAddress");
                        editor.remove("price");
                        editor.remove("address");
                        editor.remove("orderId");
                        editor.remove("size");
                        editor.remove("sizeString");
                        editor.remove("addressId");
                        editor.apply();
                        Navigation.findNavController(v).navigate(R.id.emptyOrderFragment);
                    } else {
                        Toast.makeText(view.getContext(), "Какая-то ошибка", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Order> call, Throwable t) {
                    Toast.makeText(view.getContext(), "Ошибка сервера", Toast.LENGTH_SHORT).show();
                }
            });
        });

        return view;
    }

    private int getMinuteDiff(String timeStamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date;
        try {
            date = sdf.parse(timeStamp);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            long milliCurrentTime = new Date().getTime() - 7 * 60 * 60 * 1000 + 10000;
            long milliDifference = milliCurrentTime - date.getTime();
            long minuteDifference = TimeUnit.MINUTES.convert(milliDifference, TimeUnit.MILLISECONDS);
            return (int)minuteDifference;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private int getAdditionalPrice(int minuteDifference) {
        if(minuteDifference <= 10)
            return 0;
        return (int)(minuteDifference-10)*3;
    }
}