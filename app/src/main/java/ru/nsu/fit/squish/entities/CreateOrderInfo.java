package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;

public class CreateOrderInfo {
    @Expose
    private String size;

    @Expose
    private Integer rentalPoint;

    public CreateOrderInfo(String size, Integer rentalPoint) {
        this.size = size;
        this.rentalPoint = rentalPoint;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Integer getRentalPoint() {
        return rentalPoint;
    }

    public void setRentalPoint(Integer rentalPoint) {
        this.rentalPoint = rentalPoint;
    }
}
