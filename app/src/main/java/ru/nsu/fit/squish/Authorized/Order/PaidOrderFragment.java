package ru.nsu.fit.squish.Authorized.Order;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PaidOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PaidOrderFragment extends Fragment {

    Button payButton;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PaidOrderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PaidOrderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PaidOrderFragment newInstance(String param1, String param2) {
        PaidOrderFragment fragment = new PaidOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_paid_order, container, false);
        TextView sizePaid = view.findViewById(R.id.sizePaid);
        SharedPreferences pref = this.getActivity().getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);
        int size = pref.getInt("size", 38);
        String sizeText = size + " размер";
        sizePaid.setText(sizeText);

        TextView address = view.findViewById(R.id.addressPaid);
        String addressText = pref.getString("address", "Пирогова");
        address.setText(addressText);

        TextView price = view.findViewById(R.id.paidPaid);
        int priceInt = pref.getInt("price", 50);
        String priceText = priceInt + "₽";
        price.setText(priceText);


        payButton = view.findViewById(R.id.buttonPay);
        payButton.setOnClickListener(v -> {
            //тут pay
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("state", 42);
            editor.apply();
            Navigation.findNavController(v).navigate(R.id.getOrderFragment);
        });
        return view;
    }

}