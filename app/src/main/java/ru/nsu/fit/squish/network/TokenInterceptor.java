package ru.nsu.fit.squish.network;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TokenInterceptor implements Interceptor {
    private TokenManager tokenManager;

    public TokenInterceptor(Context context) {
        this.tokenManager = new TokenManager(context);
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request original = chain.request();

        String token = tokenManager.fetchAuthToken();

        if (token == null) {
            return chain.proceed(original);
        }

        HttpUrl originalHttpUrl = original.url();
        if (originalHttpUrl.encodedPath().contains("token/") && original.method().equals("post")
                || (originalHttpUrl.encodedPath().contains("registration/") && original.method().equals("post"))
        ) {
            return chain.proceed(original);
        }

        Request.Builder requestBuilder = original.newBuilder()
                .addHeader("Authorization", "Bearer " + token)
                .url(originalHttpUrl);

        Request request = requestBuilder.build();

        return chain.proceed(request);
    }
}
