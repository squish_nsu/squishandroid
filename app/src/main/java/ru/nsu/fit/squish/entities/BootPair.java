package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;

public class BootPair {
    @Expose
    private Integer id;

    @Expose
    private String size;

    public Integer getId() {
        return id;
    }

    public String getSize() {
        return size;
    }
}
