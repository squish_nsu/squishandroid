package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;

public class Order {
    @Expose
    private Integer id;

    @Expose
    private BootPair bootPair;

    @Expose
    private String status;

    @Expose
    private StorageBox boxStarted;

    @Expose
    private StorageBox boxEnded;

    @Expose
    private String dateStarted;

    @Expose
    private String dateEnded;

    public Integer getId() {
        return id;
    }

    public BootPair getBootPair() {
        return bootPair;
    }

    public String getStatus() {
        return status;
    }

    public StorageBox getBoxStarted() {
        return boxStarted;
    }

    public StorageBox getBoxEnded() {
        return boxEnded;
    }

    public String getDateStarted() {
        return dateStarted;
    }

    public String getDateEnded() {
        return dateEnded;
    }
}
