package ru.nsu.fit.squish.Authorized.Catalog;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.CreateOrderInfo;
import ru.nsu.fit.squish.entities.RentalPoint;
import ru.nsu.fit.squish.network.Requests;

public class CatalogFragment extends Fragment {

    private Button selectedSize;
    private RentalPoint selectedAddress;
    private static final Map<String, Integer> buttonIdList = new HashMap<>();
    static {
        buttonIdList.put("36", R.id.button36);
        buttonIdList.put("37", R.id.button37);
        buttonIdList.put("38", R.id.button38);
        buttonIdList.put("39", R.id.button39);
        buttonIdList.put("40", R.id.button40);
        buttonIdList.put("41", R.id.button41);
        buttonIdList.put("42", R.id.button42);
        buttonIdList.put("43", R.id.button43);
        buttonIdList.put("44", R.id.button44);
        buttonIdList.put("45", R.id.button45);
    }

    private Spinner spinner;

    public CatalogFragment() {
        // Required empty public constructor
    }

    public static CatalogFragment newInstance() {
        CatalogFragment fragment = new CatalogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);

        Requests.getRentalPointsInfo(new Callback<List<RentalPoint>>() {
            @Override
            public void onResponse(Call<List<RentalPoint>> call, Response<List<RentalPoint>> response) {
                if(response.code() != 200){
                    Toast.makeText(view.getContext(), "Не удалось получить актуальные данные", Toast.LENGTH_SHORT).show();
                    return;
                }
                initSpinner(view, response.body());
            }

            @Override
            public void onFailure(Call<List<RentalPoint>> call, Throwable t) {
                Toast.makeText(view.getContext(), "Не удалось получить актуальные данные", Toast.LENGTH_SHORT).show();
            }
        });

        initButtons(view);
        initOrderButton(view);
        return view;
    }

    private void initButtons(View view) {
        if(selectedSize != null) {
            selectedSize.getBackground().setColorFilter(Color.parseColor("#EDEDED"), PorterDuff.Mode.MULTIPLY);
            selectedSize.setTextColor(Color.parseColor("#000000"));
        }
        for(Map.Entry<String, Integer> button : buttonIdList.entrySet()) {
            Button curButton = view.findViewById(button.getValue());
            curButton.setClickable(false);
            curButton.getBackground().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.MULTIPLY);
            curButton.setOnClickListener(v -> {
                if(selectedSize != null) {
                    selectedSize.getBackground().setColorFilter(Color.parseColor("#EDEDED"), PorterDuff.Mode.MULTIPLY);
                    selectedSize.setTextColor(Color.parseColor("#000000"));
                }

                curButton.getBackground().setColorFilter(Color.parseColor("#2535C2"), PorterDuff.Mode.MULTIPLY);
                curButton.setTextColor(Color.parseColor("#ffffff"));
                selectedSize = curButton;
            });
        }
    }

    private void blockButtons(View view) {
        for(Map.Entry<String, Integer> button : buttonIdList.entrySet()) {
            Button curButton = view.findViewById(button.getValue());
            curButton.setClickable(false);
            curButton.getBackground().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.MULTIPLY);
        }
    }

    private void initSpinner(View view, List<RentalPoint> rentalPoints) {
        spinner = view.findViewById(R.id.addresses);
        List<RentalPoint> addresses = new LinkedList<>(rentalPoints);
        addresses.add(0, new RentalPoint());
        ArrayAdapter<RentalPoint> adapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_item, addresses);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
                if(position != 0) {
                    selectedAddress = (RentalPoint) parent.getItemAtPosition(position);
                    selectedSize = null;
                    updateButtons(view, selectedAddress);
                } else {
                    blockButtons(view);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void updateButtons(View view, RentalPoint selectedAddress) {
        List<String> sizes = selectedAddress.getSizes();
        for(Map.Entry<String, Integer> button : buttonIdList.entrySet()) {
            Button curButton = view.findViewById(button.getValue());
            if(sizes.contains(button.getKey())) {
                curButton.setClickable(true);
                curButton.getBackground().setColorFilter(Color.parseColor("#EDEDED"), PorterDuff.Mode.MULTIPLY);
                curButton.setTextColor(Color.parseColor("#000000"));
            } else {
                curButton.setClickable(false);
                curButton.getBackground().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.MULTIPLY);
                curButton.setTextColor(Color.parseColor("#000000"));
            }
        }
    }

    private void initOrderButton(View view) {
        Button order = view.findViewById(R.id.orderButton);
        order.setOnClickListener(v -> {
            SharedPreferences pref = this.getActivity().getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);
            if(pref.contains("state") &&
                    pref.getInt("state", 10) != 0 && pref.getInt("state", 10) != 4) {
                Toast.makeText(v.getContext(), "Уже есть заказ в работе", Toast.LENGTH_SHORT).show();
                return;
            }

            if(selectedSize != null && selectedAddress != null && !selectedAddress.getName().isEmpty()) {

                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("state", 4);
                editor.putInt("size", Integer.parseInt(selectedSize.getText().toString()));
                editor.putString("sizeString", selectedSize.getText().toString());
                editor.putString("item", "Сапоги");
                editor.putString("address", selectedAddress.getName());
                editor.putInt("addressId", selectedAddress.getId());
                editor.apply();
                //String size = selectedSize.getText().toString();

                BottomNavigationView navigationView = v.getRootView().findViewById(R.id.bottomNav);
                navigationView.getMenu().findItem(R.id.navigationOrderFragment).setChecked(true);
                Navigation.findNavController(v).navigate(R.id.navigationOrderFragment2);
            } else {
                Toast.makeText(v.getContext(), "Выберите адрес и размер", Toast.LENGTH_SHORT).show();
            }
        });
    }
}