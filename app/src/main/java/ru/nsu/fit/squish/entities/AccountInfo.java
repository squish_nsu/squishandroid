package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.SerializedName;

public class AccountInfo {
    @SerializedName("id")
    private int id;

    @SerializedName("phone")
    private String phone;

    public int getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }
}
