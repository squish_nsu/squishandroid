package ru.nsu.fit.squish;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import ru.nsu.fit.squish.Authorized.AuthorizedActivity;
import ru.nsu.fit.squish.NotAuthorized.NotAuthorizedActivity;
import ru.nsu.fit.squish.network.Requests;
import ru.nsu.fit.squish.network.TokenManager;

public class MainActivity extends AppCompatActivity {

    public static String PREF_NAME = "app_pref";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent;
        Requests.initApi(this);

        TokenManager tokenManager = new TokenManager(this);
        String token = tokenManager.fetchAuthToken();
        if (token != null) {
            //check token
            intent = new Intent(MainActivity.this, AuthorizedActivity.class);
        } else {
            intent = new Intent(MainActivity.this, NotAuthorizedActivity.class);
        }
        startActivity(intent);
    }
}
