package ru.nsu.fit.squish.NotAuthorized;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.AuthInfo;
import ru.nsu.fit.squish.entities.LoginInfo;
import ru.nsu.fit.squish.network.Requests;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegisterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment extends Fragment {
    private static final String TAG = "RegisterFragment";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ImageButton backButton;
    private EditText phoneNumber;
    private EditText password;
    private EditText confirmPassword;
    private CheckBox accept;
    private Button registerButton;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        backButton = view.findViewById(R.id.backToLogin);
        phoneNumber = view.findViewById(R.id.editPhoneNumber);
        password = view.findViewById(R.id.editPassword);
        confirmPassword = view.findViewById(R.id.editConfirmPassword);
        accept = view.findViewById(R.id.accept);
        registerButton = view.findViewById(R.id.buttonLogin);

        backButton.setOnClickListener(v -> Navigation.findNavController(v).navigate(R.id.logInFragment));

        registerButton.setOnClickListener(v -> {
            String errors = checkFields();
            if(!errors.isEmpty()) {
                Toast.makeText(getContext(), errors, Toast.LENGTH_SHORT).show();
                return;
            }
            Requests.register(new AuthInfo(phoneNumber.getText().toString(), password.getText().toString()), new Callback<LoginInfo>() {
                @Override
                public void onResponse(Call<LoginInfo> call, Response<LoginInfo> response) {
                    Log.i(TAG, response.message());
                    if (response.code() != 200){
                        Toast.makeText(getContext(), "Ошибка регистрации", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    LoginInfo loginInfo = response.body();

                    Bundle args = new Bundle();
                    args.putString("phoneNumber", phoneNumber.getText().toString());

                    Navigation.findNavController(v).navigate(R.id.enterCode, args);
                }

                @Override
                public void onFailure(Call<LoginInfo> call, Throwable t) {
                    Toast.makeText(getContext(), "Непредвиденная ошибка.", Toast.LENGTH_SHORT).show();
                }
            });
        });


        return view;
    }

    private String checkFields(/*EditText phoneNumber, EditText password,
                                EditText confirmPassword, CheckBox accept*/) {
        //List<String> errors = new ArrayList<>();
        StringBuilder errors = new StringBuilder();
        if(phoneNumber.getText().toString().isEmpty())
            errors.append("Номер телефона пустой\n");

        if(password.getText().toString().isEmpty())
            errors.append("Пароль пустой\n");

        if(!confirmPassword.getText().toString().equals(password.getText().toString()))
            errors.append("Пароли не совпадают\n");

        if(!accept.isChecked())
            errors.append("Необходимо согласиться с политикой приложения\n");

        return errors.toString();
    }
}