package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdatePassInfo {
    @SerializedName("oldPassword")
    @Expose
    private String oldPassword;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("passwordConfirmation")
    @Expose
    private String passwordConfirmation;

    public UpdatePassInfo(String oldPassword, String password, String passwordConfirmation) {
        this.oldPassword = oldPassword;
        this.password = password;
        this.passwordConfirmation = passwordConfirmation;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
