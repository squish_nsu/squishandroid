package ru.nsu.fit.squish.Authorized.Order;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.Order;
import ru.nsu.fit.squish.network.Requests;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GetOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GetOrderFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public GetOrderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GetOrderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GetOrderFragment newInstance(String param1, String param2) {
        GetOrderFragment fragment = new GetOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_get_order, container, false);
        SharedPreferences pref = this.getActivity().getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);

        int orderId = pref.getInt("orderId", 0);
        TextView fieldNumber = view.findViewById(R.id.fieldNumberGet);
        Requests.getOrder(orderId, new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                if(response.code() == 200) {
                    fieldNumber.setText(String.valueOf(response.body().getBoxStarted().getNumber()));
                } else {
                    Toast.makeText(view.getContext(), "Заказ не выполнен. Отмена.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                Toast.makeText(view.getContext(), "Ошибка сервера", Toast.LENGTH_SHORT).show();
            }
        });

        TextView sizePaid = view.findViewById(R.id.sizeGet);
        int size = pref.getInt("size", 0);
        String sizeText = size + " размер";
        sizePaid.setText(sizeText);

        TextView address = view.findViewById(R.id.addressGet);
        String addressText = pref.getString("address", "");
        address.setText(addressText);

        TextView price = view.findViewById(R.id.paidGet);
        int priceInt = pref.getInt("price", 0);
        String priceText = priceInt + "₽";
        price.setText(priceText);

        Button openButton = view.findViewById(R.id.buttonGet);
        openButton.setOnClickListener(v -> {
            Requests.startOrder(orderId, new Callback<Order>() {
                @Override
                public void onResponse(Call<Order> call, Response<Order> response) {
                    if (response.code() == 200) {
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putInt("state", 421);
                        editor.apply();
                        Navigation.findNavController(v).navigate(R.id.withTimeFragment);
                    } else {
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putInt("state", 0);
                        editor.apply();
                        Navigation.findNavController(v).navigate(R.id.emptyOrderFragment);
                        Toast.makeText(view.getContext(), "Заказ не выполнен. Отмена.", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Order> call, Throwable t) {
                    Toast.makeText(view.getContext(), "Ошибка сервера.", Toast.LENGTH_SHORT).show();
                }
            });
        });
        return view;
    }


    private int getFieldNumber() {
        return 7;
    }
}