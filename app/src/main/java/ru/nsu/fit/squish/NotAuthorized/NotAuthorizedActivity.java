package ru.nsu.fit.squish.NotAuthorized;

import android.os.Bundle;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.Navigation;

import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.network.Requests;

public class NotAuthorizedActivity extends AppCompatActivity {

    //private NavController navController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_authorized);

        Navigation.findNavController(this, R.id.nav_not_authorized);
    }

    public void redirectToRegister(View view) {
        Navigation.findNavController(view).navigate(R.id.registerFragment);
    }

}