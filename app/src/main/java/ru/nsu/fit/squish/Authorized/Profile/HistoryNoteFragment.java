package ru.nsu.fit.squish.Authorized.Profile;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.nsu.fit.squish.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HistoryNoteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryNoteFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String WHEN_PARAM = "when";
    private static final String WHAT_PARAM = "what";
    private static final String ADD_PARAM = "add";

    // TODO: Rename and change types of parameters
    private String whenParam;
    private String whatParam;
    private String addParam;

    public HistoryNoteFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param when Parameter 1.
     * @param what Parameter 2.
     * @return A new instance of fragment HistoryNoteFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HistoryNoteFragment newInstance(String when, String what, String additionalInfo) {
        HistoryNoteFragment fragment = new HistoryNoteFragment();
        Bundle args = new Bundle();
        args.putString(WHEN_PARAM, when);
        args.putString(WHAT_PARAM, what);
        args.putString(ADD_PARAM, additionalInfo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            whenParam = getArguments().getString(WHEN_PARAM);
            whatParam = getArguments().getString(WHAT_PARAM);
            addParam = getArguments().getString(ADD_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_note, container, false);

        TextView when = view.findViewById(R.id.when);
        when.setText(whenParam);

        TextView what = view.findViewById(R.id.what);
        what.setText(whatParam);

        TextView add = view.findViewById(R.id.additionalInfo);
        add.setText(addParam);

        return view;
    }
}