package ru.nsu.fit.squish.network;

import android.content.Context;
import android.content.SharedPreferences;

import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;

/**
 * Session manager to save and fetch data from SharedPreferences
 */
public class TokenManager {
    private static final String USER_TOKEN = "user_token";
    private static final String USER_ID = "user_id";

    SharedPreferences prefs;

    public TokenManager(Context context) {
        prefs = context.getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Function to save auth token
     */
    public void saveAuthToken(String token) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_TOKEN, token);
        editor.apply();
    }

    /**
     * Function to fetch auth token
     */
    public String fetchAuthToken() {
        return prefs.getString(USER_TOKEN, null);
    }

    public void clearAuthToken() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(USER_TOKEN);
        editor.apply();
    }

    public void saveId(int id) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(USER_ID, id);
        editor.apply();
    }

    public int fetchId() {
        return prefs.getInt(USER_ID, 0);
    }

    public void clearId() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(USER_ID);
        editor.apply();
    }
}