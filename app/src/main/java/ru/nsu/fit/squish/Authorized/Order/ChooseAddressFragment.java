package ru.nsu.fit.squish.Authorized.Order;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.EndOrderInfo;
import ru.nsu.fit.squish.entities.Order;
import ru.nsu.fit.squish.entities.RentalPoint;
import ru.nsu.fit.squish.entities.StorageBox;
import ru.nsu.fit.squish.network.Requests;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChooseAddressFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChooseAddressFragment extends Fragment {

    private RentalPoint selectedAddress;
    private String fieldNumber;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private SharedPreferences pref;
    private StorageBox box;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ChooseAddressFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChooseAddressFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChooseAddressFragment newInstance(String param1, String param2) {
        ChooseAddressFragment fragment = new ChooseAddressFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_choose_address, container, false);
        pref = this.getActivity().getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);
        int orderId = pref.getInt("orderId", 0);
        Requests.getRentalPointsInfo(new Callback<List<RentalPoint>>() {
            @Override
            public void onResponse(Call<List<RentalPoint>> call, Response<List<RentalPoint>> response) {
                if(response.code() != 200){
                    Toast.makeText(view.getContext(), "Не удалось получить актуальные данные", Toast.LENGTH_SHORT).show();
                    return;
                }
                initSpinner(view, response.body());
            }

            @Override
            public void onFailure(Call<List<RentalPoint>> call, Throwable t) {
                Toast.makeText(view.getContext(), "Не удалось получить актуальные данные", Toast.LENGTH_SHORT).show();
            }
        });

        TextView sizeView = view.findViewById(R.id.sizeChooseAddress);
        int size = pref.getInt("size", 38);
        String sizeText = size + " размер";
        sizeView.setText(sizeText);

        Button openField = view.findViewById(R.id.openField);
        openField.setOnClickListener(v-> {
            if(selectedAddress == null || selectedAddress.getName().isEmpty()) {
                Toast.makeText(v.getContext(), "Выберите адрес сдачи заказа", Toast.LENGTH_SHORT).show();
                return;
            }
            if(box != null) {
                EndOrderInfo endOrderInfo = new EndOrderInfo(box.getId());
                Requests.endOrder(orderId, endOrderInfo, new Callback<Order>() {
                    @Override
                    public void onResponse(Call<Order> call, Response<Order> response) {
                        if (response.code() == 200) {
                            pref.edit().putInt("state", 45).apply();
                            Navigation.findNavController(v).navigate(R.id.passOrderFragment);
                        } else {
                            Toast.makeText(v.getContext(), "Какая-то ошибка. Уточняем", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Order> call, Throwable t) {
                        Toast.makeText(v.getContext(), "Ошибка сервера", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        return view;
    }

    private void initSpinner(View view, List<RentalPoint> rentalPoints) {
        Spinner spinner = view.findViewById(R.id.addressesBack);
        List<RentalPoint> addresses = new LinkedList<>(rentalPoints);
        addresses.add(0,new RentalPoint());
        ArrayAdapter<RentalPoint> adapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_spinner_item, addresses);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        TextView fieldNum = view.findViewById(R.id.fieldNumber);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position != 0) {
                    selectedAddress = (RentalPoint)parent.getItemAtPosition(position);
                    pref.edit().putString("selectedAddress", selectedAddress.getName()).apply();
                    box = getFieldNumber(selectedAddress);
                    fieldNumber = box.getNumber();
                    fieldNum.setText(fieldNumber);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private StorageBox getFieldNumber(RentalPoint rentalPointId) {
        List<StorageBox> boxes = rentalPointId.getFreeBoxes();
        if(!boxes.isEmpty()) {
            return boxes.get(0);
        }
        return null;
    }
}