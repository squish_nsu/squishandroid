package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;

public class EndOrderInfo {
    @Expose
    private Integer box;

    public EndOrderInfo(Integer box) {
        this.box = box;
    }

    public Integer getBox() {
        return box;
    }

    public void setBox(Integer box) {
        this.box = box;
    }
}
