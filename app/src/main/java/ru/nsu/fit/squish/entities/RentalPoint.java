package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RentalPoint {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("sizes")
    @Expose
    private List<String> sizes;

    @SerializedName("freeBoxes")
    @Expose
    private List<StorageBox> freeBoxes;

    public RentalPoint() {
    }

    public int getId() {
        return id;
    }

    public List<String> getSizes() {
        return sizes;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public List<StorageBox> getFreeBoxes() {
        return freeBoxes;
    }

    @NotNull
    @Override
    public String toString() {
        return name;
    }
}
