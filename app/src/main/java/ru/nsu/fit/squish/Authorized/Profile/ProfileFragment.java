package ru.nsu.fit.squish.Authorized.Profile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.AccountInfo;
import ru.nsu.fit.squish.network.Requests;
import ru.nsu.fit.squish.network.TokenManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {


    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using getPhoneNumberthe provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        TextView phoneNumberValue = view.findViewById(R.id.phoneNumberValue);
        TokenManager tokenManager1 = new TokenManager(view.getContext());
        Requests.accountsRead(tokenManager1.fetchId(), new Callback<AccountInfo>() {
            @Override
            public void onResponse(Call<AccountInfo> call, Response<AccountInfo> response) {
                if(response.code() == 200) {
                    phoneNumberValue.setText(response.body().getPhone());
                }
            }

            @Override
            public void onFailure(Call<AccountInfo> call, Throwable t) {
                Toast.makeText(view.getContext(), "Ошибка сервера", Toast.LENGTH_SHORT).show();
            }
        });

        ConstraintLayout layout = view.findViewById(R.id.phoneNumberButton);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.phoneNumberFragment);
            }
        });
        ConstraintLayout changePassword = view.findViewById(R.id.buttonChangePassword);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.changePasswordFragment);
            }
        });

        ConstraintLayout logout = view.findViewById(R.id.logoutButton);
        logout.setOnClickListener(v -> {
            TokenManager tokenManager = new TokenManager(v.getContext());
            tokenManager.clearAuthToken();
            tokenManager.clearId();
            Navigation.findNavController(v).navigate(R.id.notAuthorizedActivity);
        });

        ConstraintLayout history = view.findViewById(R.id.orderHistoryButton);
        history.setOnClickListener(v -> {
            Navigation.findNavController(v).navigate(R.id.historyFragment);
        });
        return view;
    }
}