package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthInfo {
    public AuthInfo(String phoneNumber, String password) {
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    @SerializedName("phone")
    @Expose
    private String phoneNumber;

    @Expose
    private String password;
}
