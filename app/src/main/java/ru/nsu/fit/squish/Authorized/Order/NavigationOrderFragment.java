package ru.nsu.fit.squish.Authorized.Order;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NavigationOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NavigationOrderFragment extends Fragment {

    public NavigationOrderFragment() {
    }

    public static NavigationOrderFragment newInstance() {
        NavigationOrderFragment fragment = new NavigationOrderFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_order, container, false);
        checkState(view);
        return view;
    }

    private void checkState(View view) {
        NavController navController = Navigation.findNavController(view.findViewById(R.id.nav_order));
        SharedPreferences pref = this.getActivity().getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);
        if (pref.contains("state")) {
            switch (pref.getInt("state", 0)) {
                case 0:
                    navController.navigate(R.id.emptyOrderFragment);
                    return;
                case 4:
                    navController.navigate(R.id.currentOrderFragment);
                    return;
                case 41:
                    navController.navigate(R.id.paidOrderFragment);
                    return;
                case 42:
                    navController.navigate(R.id.getOrderFragment);
                    return;
                case 421:
                    navController.navigate(R.id.withTimeFragment);
                    return;
                case 43:
                    navController.navigate(R.id.chooseAddressFragment);
                    return;
                case 45:
                    navController.navigate(R.id.passOrderFragment);
                    return;
            }
        }
    }
}