package ru.nsu.fit.squish.Authorized.Order;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.CreateOrderInfo;
import ru.nsu.fit.squish.entities.Order;
import ru.nsu.fit.squish.network.Requests;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CurrentOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CurrentOrderFragment extends Fragment {

    private CheckBox getNowCheckbox;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public CurrentOrderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CurrentOrderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CurrentOrderFragment newInstance(String param1, String param2) {
        CurrentOrderFragment fragment = new CurrentOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_order, container, false);
        TextView priceView  = view.findViewById(R.id.price);
        SharedPreferences pref = this.getActivity().getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);
        getNowCheckbox = view.findViewById(R.id.getRightNow);
        getNowCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    int price = getPrice(new Date());
                    String priceText = price + "₽";
                    priceView.setText(priceText);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putInt("price", price);
                    editor.apply();

                } else {
                    priceView.setText("");
                }
            }
        });

        int size = pref.getInt("size", 38);
        TextView sizeView = view.findViewById(R.id.size);
        String sizeText = size + " размер";
        sizeView.setText(sizeText);
        String address = pref.getString("address", "Пирогова");
        TextView addressView = view.findViewById(R.id.address);
        addressView.setText(address);

        buttonHandler(view);
        return view;
    }

    private int getPrice(Date date) {
        Date currentDate = new Date();

        long diff = TimeUnit.DAYS.convert(date.getTime() - currentDate.getTime(), TimeUnit.MILLISECONDS);
        return 30 + (int)diff*10;
    }

    private void buttonHandler(View view) {
        Button payButton = view.findViewById(R.id.pay);
        payButton.setOnClickListener(v -> {
            if(getNowCheckbox.isChecked()) {
                SharedPreferences pref = this.getActivity().getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);
                CreateOrderInfo createOrderInfo = new CreateOrderInfo(pref.getString("sizeString", ""), pref.getInt("addressId", 0));
                Requests.createOrder(createOrderInfo, new Callback<Order>() {
                    @Override
                    public void onResponse(Call<Order> call, Response<Order> response) {
                        if(response.code() == 200) {
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putInt("orderId", response.body().getId());
                            editor.putInt("state", 41);
                            editor.apply();
                            Navigation.findNavController(v).navigate(R.id.paidOrderFragment);
                        } else {
                            Toast.makeText(v.getContext(), "Произошла ошибка", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Order> call, Throwable t) {
                        Toast.makeText(v.getContext(), "Произошла ошибка", Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                Toast.makeText(v.getContext(), "Необходимо выбрать время выдачи", Toast.LENGTH_SHORT).show();
            }
        });
    }
}