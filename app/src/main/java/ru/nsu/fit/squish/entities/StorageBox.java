package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;

public class StorageBox {
    @Expose
    private Integer id;

    @Expose
    private String number;

    public Integer getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }
}
