package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginInfo {
    @Expose
    private Integer userId;

    @SerializedName("phone")
    @Expose
    private String phoneNumber;

    public Integer getUserId() {
        return userId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
