package ru.nsu.fit.squish.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfirmInfo {
    @SerializedName("phone")
    @Expose
    private String phoneNumber;

    @Expose
    private String code;

    public ConfirmInfo(String phoneNumber, String code) {
        this.phoneNumber = phoneNumber;
        this.code = code;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
