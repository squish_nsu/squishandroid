package ru.nsu.fit.squish.NotAuthorized;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.AuthInfo;
import ru.nsu.fit.squish.entities.TokenResponse;
import ru.nsu.fit.squish.network.Requests;
import ru.nsu.fit.squish.network.TokenManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LogInFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LogInFragment extends Fragment {
    private static final String TAG = "LogInFragment";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button login;

    public LogInFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LogInFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LogInFragment newInstance(String param1, String param2) {
        LogInFragment fragment = new LogInFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log_in, container, false);
        login = view.findViewById(R.id.buttonLogin);
        login.setOnClickListener(v -> {
            EditText editPhoneNumber = view.findViewById(R.id.editPhoneNumber);
            String phoneNumber = editPhoneNumber.getText().toString();

            EditText editPassword = view.findViewById(R.id.editPassword);
            String password = editPassword.getText().toString();

            TokenManager tokenManager = new TokenManager(v.getContext());

            Requests.login(new AuthInfo(phoneNumber, password), new Callback<TokenResponse>() {
                @Override
                public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                    Log.i(TAG, Integer.toString(response.code()));
                    if (response.code() == 400 || response.code() == 401) {
                        Toast.makeText(getContext(), "Неправильный логин и/или пароль.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    tokenManager.saveId(response.body().getId());
                    tokenManager.saveAuthToken(response.body().getAccess());
                    Navigation.findNavController(v).navigate(R.id.authorizedActivity);
                }

                @Override
                public void onFailure(Call<TokenResponse> call, Throwable t) {
                    Toast.makeText(getContext(), "Непредвиденная ошибка.", Toast.LENGTH_SHORT).show();
                }
            });
        });
        return view;

    }
}