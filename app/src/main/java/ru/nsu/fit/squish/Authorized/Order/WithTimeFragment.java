package ru.nsu.fit.squish.Authorized.Order;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.MainActivity;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.Order;
import ru.nsu.fit.squish.network.Requests;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WithTimeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WithTimeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    long minuteDifference;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public WithTimeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WithTimeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WithTimeFragment newInstance(String param1, String param2) {
        WithTimeFragment fragment = new WithTimeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_with_time, container, false);
        TextView item = view.findViewById(R.id.size3);
        SharedPreferences pref = this.getActivity().getSharedPreferences(MainActivity.PREF_NAME, Context.MODE_PRIVATE);

        TextView timeView = view.findViewById(R.id.currentTime);
        TextView priceView = view.findViewById(R.id.currentPrice);

        int orderId = pref.getInt("orderId", 0);
        Requests.getOrder(orderId, new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                if(response.code() == 200) {
                    Order curOrder = response.body();
                    String time = getCurrentTime(curOrder.getDateStarted());
                    timeView.setText(time);

                    int price = getCurrentPrice();
                    String priceText = price + "₽";
                    priceView.setText(priceText);
                } else {
                    Toast.makeText(view.getContext(), "Произошла какая-то ошибка", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                Toast.makeText(view.getContext(), "Ошибка сервера", Toast.LENGTH_SHORT).show();
            }
        });

        int size = pref.getInt("size", 38);
        String sizeText = size + " размер";
        item.setText(sizeText);

        Button passItem = view.findViewById(R.id.passItem);
        passItem.setOnClickListener(v -> {
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("state", 43);
            editor.apply();
            Navigation.findNavController(v).navigate(R.id.chooseAddressFragment);
        });

        return view;
    }

    private String getCurrentTime(String timeStamp) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date;
        try {
            date = sdf.parse(timeStamp);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date currentTime = new Date();
            long milliCurrentTime = currentTime.getTime() - 7 * 60 * 60 * 1000 + 10000;
            long milliDifference = milliCurrentTime - date.getTime();
            minuteDifference = TimeUnit.MINUTES.convert(milliDifference, TimeUnit.MILLISECONDS);
            long seconds = TimeUnit.SECONDS.convert(milliDifference, TimeUnit.MILLISECONDS) % 60;
            return String.valueOf(minuteDifference) + ":" + String.valueOf(seconds);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    private int getCurrentPrice() {
        if(minuteDifference <= 10)
            return 0;
        return (int)(minuteDifference - 10)*3;
    }
}