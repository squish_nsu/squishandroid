package ru.nsu.fit.squish.Authorized.Profile;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.ConfirmInfo;
import ru.nsu.fit.squish.entities.TokenResponse;
import ru.nsu.fit.squish.network.Requests;
import ru.nsu.fit.squish.network.TokenManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ConfirmNumberFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConfirmNumberFragment extends Fragment {
    private static final String TAG = "ConfirmNumberFragment";

    private static final String ARG_PHONE_NUMBER = "phoneNumber";

    private String phoneNumber;

    public ConfirmNumberFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param phoneNumber Parameter 1 - phone number.
     * @return A new instance of fragment ConfirmNumberFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConfirmNumberFragment newInstance(String phoneNumber) {
        ConfirmNumberFragment fragment = new ConfirmNumberFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PHONE_NUMBER, phoneNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            phoneNumber = getArguments().getString(ARG_PHONE_NUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_confirm_number, container, false);
        ImageButton back = view.findViewById(R.id.backToPrifile3);
        back.setOnClickListener(v -> {
            Navigation.findNavController(v).navigate(R.id.phoneNumberFragment);
        });

        EditText code = view.findViewById(R.id.enterConfirmCode);

        Button enter = view.findViewById(R.id.buttonSetNewPhone);
        enter.setOnClickListener(v -> {
            if(code.getText().toString().isEmpty()) {
                Toast.makeText(v.getContext(), "Введите код подтверждения", Toast.LENGTH_SHORT).show();
                return;
            }

            TokenManager tokenManager = new TokenManager(v.getContext());

            Requests.confirm(new ConfirmInfo(phoneNumber, code.getText().toString()), new Callback<TokenResponse>() {
                @Override
                public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                    if (response.code() == 400) {
                        Toast.makeText(getContext(), "Неправильный код подтверждения.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    tokenManager.saveId(response.body().getId());
                    tokenManager.saveAuthToken(response.body().getAccess());
                    Navigation.findNavController(v).navigate(R.id.profileFragment2);
                }

                @Override
                public void onFailure(Call<TokenResponse> call, Throwable t) {
                    Toast.makeText(getContext(), "Непредвиденная ошибка.", Toast.LENGTH_SHORT).show();
                }
            });
        });

        return view;
    }
}