package ru.nsu.fit.squish.Authorized.Profile;

import android.media.Image;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.nsu.fit.squish.R;
import ru.nsu.fit.squish.entities.AuthInfo;
import ru.nsu.fit.squish.entities.LoginInfo;
import ru.nsu.fit.squish.entities.TokenResponse;
import ru.nsu.fit.squish.entities.UpdatePassInfo;
import ru.nsu.fit.squish.network.Requests;
import ru.nsu.fit.squish.network.TokenManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChangePasswordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChangePasswordFragment extends Fragment {
    private static final String TAG = "ChangePasswordFragment";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ImageButton backButton;
    private Button onChangePassword;

    private EditText oldPassword;
    private EditText newPassword;
    private EditText confirmNewPassword;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChangePasswordFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChangePasswordFragment newInstance(String param1, String param2) {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);

        oldPassword = view.findViewById(R.id.oldPassword);
        newPassword = view.findViewById(R.id.newPassword);
        confirmNewPassword = view.findViewById(R.id.confirmNewPassword);

        backButton = view.findViewById(R.id.backToProfile);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.profileFragment2);
            }
        });

        onChangePassword = view.findViewById(R.id.onChangePassword);

        onChangePassword.setOnClickListener(v -> {
            String errors = checkFields();
            if (!errors.isEmpty()) {
                Toast.makeText(getContext(), errors, Toast.LENGTH_SHORT).show();
                return;
            }
            TokenManager tokenManager = new TokenManager(view.getContext());
            Requests.updatePassword(
                new UpdatePassInfo(
                        oldPassword.getText().toString(),
                        newPassword.getText().toString(),
                        confirmNewPassword.getText().toString()
                ),
                new Callback<TokenResponse>() {
                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        Log.i(TAG, response.message());
                        if (response.code() != 200) {
                            Toast.makeText(getContext(), "Ошибка смены пароля", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        tokenManager.saveAuthToken(response.body().getAccess());
                        Navigation.findNavController(v).navigate(R.id.profileFragment2);
                    }


                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        Toast.makeText(getContext(), "Непредвиденная ошибка.", Toast.LENGTH_SHORT).show();
                    }
                });
        });


        // Inflate the layout for this fragment
        return view;
    }

    private String checkFields() {
        StringBuilder errors = new StringBuilder();
        if(oldPassword.getText().toString().isEmpty())
            errors.append("Старый пароль пустой\n");

        if(newPassword.getText().toString().isEmpty())
            errors.append("Новый пароль пустой\n");

        if(!confirmNewPassword.getText().toString().equals(newPassword.getText().toString()))
            errors.append("Пароли не совпадают\n");

        return errors.toString();
    }
}