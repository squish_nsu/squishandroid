package ru.nsu.fit.squish.network;

import android.content.Context;
import android.icu.number.Scale;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.nsu.fit.squish.entities.AccountInfo;
import ru.nsu.fit.squish.entities.AuthInfo;
import ru.nsu.fit.squish.entities.ConfirmInfo;
import ru.nsu.fit.squish.entities.CreateOrderInfo;
import ru.nsu.fit.squish.entities.EndOrderInfo;
import ru.nsu.fit.squish.entities.LoginInfo;
import ru.nsu.fit.squish.entities.Order;
import ru.nsu.fit.squish.entities.RentalPoint;
import ru.nsu.fit.squish.entities.TokenResponse;
import ru.nsu.fit.squish.entities.UpdatePassInfo;

public class Requests {

    private static final String BASE_URL = "http://84.201.162.56:8001/api/";
    private static SquishApi api;

    public static void initApi(Context context) {
        if (api != null) {
            return;
        }

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        TokenInterceptor tokenInterceptor = new TokenInterceptor(context);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(tokenInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(   ))
                .build();

        api = retrofit.create(SquishApi.class);
    }

    private Requests() {
    }

    public static List<String> getAddresses() {
        List<String> addresses = new ArrayList<>();

        api.getAddresses().enqueue(new Callback<List<RentalPoint>>() {
            @Override
            public void onResponse(Call<List<RentalPoint>> call, Response<List<RentalPoint>> response) {
                for (RentalPoint point : (List<RentalPoint>) response.body()) {
                    addresses.add(point.getName());
                }
            }

            @Override
            public void onFailure(Call<List<RentalPoint>> call, Throwable t) {
                t.printStackTrace();
            }
        });

        return addresses;
    }

    public static String getPhoneNumber() {
        return "+79997771122";
    }

    public static void login(AuthInfo authInfo, Callback<TokenResponse> callback) {
        api.login(authInfo).enqueue(callback);
    }

    public static void refresh(String refresh) {
        api.refresh(refresh).enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {

            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {

            }
        });
    }

    public static void register(AuthInfo authInfo, Callback<LoginInfo> callback) {
        api.register(authInfo).enqueue(callback);
    }

    public static void confirm(ConfirmInfo confirmInfo, Callback<TokenResponse> callback) {
        api.confirm(confirmInfo).enqueue(callback);
    }

    public static void updatePassword(UpdatePassInfo updatePassInfo, Callback<TokenResponse> callback){
        api.updatePassword(updatePassInfo).enqueue(callback);
    }

    public static void updatePhone(int myId, AuthInfo authInfo, Callback<AuthInfo> callback) {
        api.updatePhone(myId, authInfo).enqueue(callback);
    }

    public static void getRentalPointsInfo(Callback<List<RentalPoint>> callback) {
        api.getAddresses().enqueue(callback);
    }

    public static void createOrder(CreateOrderInfo orderInfo, Callback<Order> callback) {
        api.createOrder(orderInfo).enqueue(callback);
    }

    public static void listOrders(Callback<List<Order>> callback) {
        api.listOrders().enqueue(callback);
    }

    public static void getOrder(int id, Callback<Order> callback) {
        api.getOrder(id).enqueue(callback);
    }

    public static void startOrder(int id, Callback<Order> callback) {
        api.startOrder(id).enqueue(callback);
    }

    public static void endOrder(int id, EndOrderInfo endOrderInfo, Callback<Order> callback) {
        api.endOrder(id, endOrderInfo).enqueue(callback);
    }

    public static void payOrder(int id, Callback<Order> callback) {
        api.payOrder(id).enqueue(callback);
    }

    public static void accountsRead(int id, Callback<AccountInfo> callback) {
        api.accountsRead(id).enqueue(callback);
    }
}